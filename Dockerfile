FROM golang:1.19-alpine as build

WORKDIR /app

COPY . ./

RUN GO111MODULE=on go build ./cmd/hydroxide

FROM alpine:3.17

WORKDIR /app

COPY --from=build /app/hydroxide ./

RUN adduser -D go && chown -R go:go /app

USER go

# smtp
EXPOSE 1025

# imap
EXPOSE 1143

# carddav
EXPOSE 8080

ENV HOME /app

VOLUME /app/.config/hydroxide

CMD ./hydroxide -smtp-host 0.0.0.0 -smtp-port 1025 \
    -imap-host 0.0.0.0 -imap-port 1143 \
    -carddav-host 0.0.0.0 -carddav-port 8080 \
    serve
